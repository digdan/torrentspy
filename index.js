var fs = require('fs');
var async = require('async');
var readTorrent = require('read-torrent');
var request = require('request');
var options = {
	url : 'http://bitsnoop.com/api/latest_tz.php?t=verified',
	headers: {
		'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0'
	}
};

function lineScraper(parts) {
	var torrent = []
	var hash = parts[0];
	console.log(parts);
	process.exit();
	readTorrent('http://torcache.net/torrent/'+hash+'.torrent', function(error,torrent) {
		if (!error) {
			//Remove uneeded information
			delete torrent.infoBuffer;
			delete torrent.pieces;
			delete torrent.info;
			delete torrent.announce;
			delete torrent['announce-list'];
			torrent.discovered = Math.floor(Date.now() / 1000);
			torrent.name = parts[1];
			torrent.category = parts[2];				
			console.log('Storing '+hash+' into DB.');
			console.dir(torrent);
		} else {
			//Try to grab info directly from bitsnoop
			console.log('Error processing '+hash+' : '+error);
		}
	});		
}

function scrapeSnoop() {
	request(options, function(error,response,html) {
		var tasks=[];
		if (!error) {
			var lines = html.split("\n");
			lines.forEach( function(line) {
				var parts = line.split("|");
				if (parts.length > 4) {
					tasks.push( function(callback) { 
						try {
							lineScraper(parts);
						}
						catch (error) {
							console.log('Error processing '+parts[0]);
						}
						callback();
					} );				
				}
			});
			console.log('Found '+lines.length+' listings')
			async.parallel(tasks,function(error){
				if (error) {
					throw error;
				} else {
					console.log('Tasks completed.');
				}
			});
		} else {
			throw error;
		}
	});
}

module.exports = scrapeSnoop;